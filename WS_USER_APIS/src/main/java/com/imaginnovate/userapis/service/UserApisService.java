package com.imaginnovate.userapis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imaginnovate.userapis.entity.UserApis;
import com.imaginnovate.userapis.repository.UserApisRepository;

@Service
public class UserApisService {

	@Autowired
	UserApisRepository userApisRepository;
	
	public UserApis createUser(UserApis userApis) {
		UserApis newUser =userApisRepository.save(userApis);
		return newUser;
	}
	
	public UserApis getUserByEmail(String email) {
		UserApis user = userApisRepository.findUserByEmail(email);
		return user;
	}
	
	public List<UserApis> getAllUsers(){
		List<UserApis> listOfUsers = userApisRepository.findAll();
		return listOfUsers;
	}
}
