package com.imaginnovate.userapis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.imaginnovate.userapis.entity.UserApis;
import com.imaginnovate.userapis.service.UserApisService;

@RestController
@RequestMapping("/userapis")
public class UserApisController {
	
	@Autowired
	UserApisService userApisService;
	
	@PostMapping("/createUser")
	public UserApis createUser(@RequestBody UserApis userApis) {
		return userApisService.createUser(userApis);
	}
	
	@GetMapping("/getAllUsers")
	public List<UserApis> getAllUsers(){
		return userApisService.getAllUsers();
	}
	
	@GetMapping("/getUserByMail/{email}")
	public UserApis getUserByMail(@PathVariable String email) {
		return userApisService.getUserByEmail(email);
	}
}
