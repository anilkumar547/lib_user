package com.imaginnovate.userapis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsUserApisApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsUserApisApplication.class, args);
	}

}
