package com.imaginnovate.userapis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.imaginnovate.userapis.entity.UserApis;

@Repository
public interface UserApisRepository  extends JpaRepository<UserApis, Integer>{
	
	UserApis findUserByEmail(String email);

}
