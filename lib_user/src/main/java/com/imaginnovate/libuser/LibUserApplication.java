package com.imaginnovate.libuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibUserApplication.class, args);
	}

}
