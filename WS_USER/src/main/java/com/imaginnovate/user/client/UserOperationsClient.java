package com.imaginnovate.user.client;

import org.springframework.stereotype.Service;

import com.imaginnovate.libuser.client.UserClient;
import com.imaginnovate.libuser.dto.User;

@Service
public class UserOperationsClient {

	UserClient userClient  = new UserClient();
	
	public User createuser(User user) {
		return userClient.createUser(user);
	}
}
