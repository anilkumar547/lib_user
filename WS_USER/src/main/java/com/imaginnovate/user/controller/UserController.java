package com.imaginnovate.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.imaginnovate.libuser.dto.User;
import com.imaginnovate.user.client.UserOperationsClient;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserOperationsClient userOperationsClient;
	
	@PostMapping("/addUser")
	public User createUser(@RequestBody User user) {
		return userOperationsClient.createuser(user);
	}

}





//@Bean
//public RestTemplate getRestTemplate() {
//	return new RestTemplate();
//}
//
//@Autowired
//private RestTemplate restTemplate;
//
//static final String USER_APIS_URL = "http://localhost:8081/userapis/";
//
//@GetMapping("/test")
//public String test() {
//	return "test";
//}
//
//@GetMapping("/userBymail/{email}")
//public String getUserByEmail(@PathVariable String email) {
//	User user = restTemplate.exchange(USER_APIS_URL+"getUserByMail/"+email, HttpMethod.GET, null, User.class).getBody();
//	return restTemplate.getForObject(USER_APIS_URL+"getUserByMail/"+email, String.class);
//}
//
//@PostMapping("/addUser")
//public User addUser(@RequestBody User user) {
//	return restTemplate.postForObject(USER_APIS_URL+"createUser", user, User.class);
//}
